package it.unibo.oop.mnk;

import java.util.Optional;

public class MatchEventArgs extends MNKEventArgs {

    public MatchEventArgs(MNKMatch source, int turn, Symbols winner, int i, int j) {
        super(source, turn, winner, i, j);
    }

    public Optional<Symbols> getWinner() {
        return Optional.ofNullable(getPlayer());
    }
}
